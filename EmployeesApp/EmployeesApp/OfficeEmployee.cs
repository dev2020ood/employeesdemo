﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmployeesApp
{
    class OfficeEmployee : Employee
    {
        int _rank;
        int _hours; 
        public override double CalcSal()
        {
            return _rank * _hours; 
        }
    }
}
